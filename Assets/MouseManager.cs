﻿using UnityEngine;
using System.Collections;

public class MouseManager : MonoBehaviour {

    public Boid selectedBoid;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            if ( Input.GetMouseButtonDown(0) ) 
            {
                Boid boid = hitInfo.collider.GetComponent<Boid>();
                if (boid != null)
                {
                    selectedBoid = boid;
                }
            }
        }

	}
}
