﻿using UnityEngine;
using System.Collections.Generic;

public class BoidsManager : MonoBehaviour {

    public int maxNumBoids = 2;
    public GameObject boidPrefab;

    Boid[] boids;

	// Use this for initialization
	void Start () {
        boids = new Boid[maxNumBoids];
        Transform boidsParent = new GameObject("Boids").transform;
        for (int i = 0; i < boids.Length; i++) {
            GameObject boid = ( GameObject ) Instantiate(boidPrefab, Random.insideUnitSphere, Random.rotation);
            boid.transform.SetParent(boidsParent);
            boid.name = "Boid" + i; 
                
            boids[i] = boid.GetComponent<Boid>();
            boids[i].targetPosition = Random.insideUnitSphere * 10f;
        }

	}

    void Update() {
        Boid boid;
        for (int i = 0; i < boids.Length; i++)
        {
            boid = boids[i];
            boid.DoUpdate();

            if (boid.flockSize == 0)
            {
                boids[i].targetPosition = Random.insideUnitSphere * 10f;
            }
        }
    }



}
