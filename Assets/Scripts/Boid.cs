﻿using UnityEngine;
using System.Collections.Generic;

public class Boid : MonoBehaviour {

    public float maxSpeed = 100f;
    public float avoidDistance = 1.3f;
    public float avoidPredatorBase = 1f;
    public float rotationSpeed = 5f;
    public float flockMaxDistance = 2f;
    public float maxRuleDelay = 5f;
    public int flockSize;

    Boid[] boids;
    Predator[] predators;
    Vector3 averagePosition;
    Vector3 _velocity;
    public Vector3 targetPosition;

    Boid[] myFlock;

    Boid boid;

    public float currentAdrenaline = 0;
    float maxAdrenaline = 10f;
    float coolDown;

    // Use this for initialization
    void Start () {
        boids = GameObject.FindObjectsOfType<Boid>();
        predators = GameObject.FindObjectsOfType<Predator>();
    }

    // Update is called once per frame
    public void DoUpdate () 
    {
        // add all rules to velocity
        if (Random.Range(0, maxRuleDelay - (currentAdrenaline/23f * 5f)) < 1)
        {
            _velocity = Vector3.zero;
            _velocity += 1.0f * MoveToAvgPositionOtherBoids();
            _velocity += 1.0f * AvoidCollision();
            _velocity += 0.4f * AdaptToVelocityOfGroup();
            _velocity += 1.0f * MoveToTarget();
        }

        // always be wary of predators, so if they are in range, act immediately.
        _velocity += 5.0f * AvoidPredator();

        // generate adrenaline modifier based on velocity
        float rawAdrenaline = (currentAdrenaline / maxAdrenaline) * maxSpeed;
        if (rawAdrenaline <= _velocity.magnitude)
        {
            currentAdrenaline = _velocity.magnitude / (maxSpeed * maxAdrenaline);
        }
        else
        {
            currentAdrenaline -= 12f * Time.deltaTime;
            _velocity = _velocity.normalized * currentAdrenaline;
        }

        if (_velocity.magnitude > maxSpeed)
        {
            _velocity = _velocity.normalized * maxSpeed;
        }

        transform.Translate(0, 0, _velocity.magnitude * Time.deltaTime);

        float rotationMultiplier = 1f;
        if (currentAdrenaline > 10)
        {
            rotationMultiplier = 1.5f;
        }

        if (_velocity != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(_velocity),
                rotationSpeed * rotationMultiplier * Time.deltaTime);
        }
    }

    Vector3 MoveToTarget() 
    {
//        Debug.DrawLine(transform.position, target.transform.position, Color.blue);
        return targetPosition - transform.position;
    }

    Vector3 MoveToAvgPositionOtherBoids() 
    {
        Vector3 avgPositionOtherBoids = Vector3.zero;
        flockSize = 0;

        for (int i = 0; i < boids.Length; i++) {
            boid = boids[i];
            if (boid != this)
            {
                // if its within flockingDistance, we use its position
                if (Vector3.Distance(transform.position, boid.transform.position) < flockMaxDistance)
                {
                    flockSize++;
                    avgPositionOtherBoids += boid.transform.position;

                    // adapt position from other boid if they belong to a bigger flock
                    if (boid.flockSize > flockSize)
                    {
                        targetPosition = boid.targetPosition;
                    }
                }
            }
        }

        if (flockSize == 0)
        {
            
            return Vector3.zero;
        }

        return (avgPositionOtherBoids / flockSize) - transform.position;

    }



    Vector3 AvoidCollision() 
    {
        Vector3 c = Vector3.zero;

        for (int i = 0; i < boids.Length; i++) {
            boid = boids[i];
            if (boid != this)
            {
                if (Vector3.Distance(boid.transform.position, this.transform.position) < avoidDistance)
                {
                    float m = avoidDistance - Vector3.Distance(boid.transform.position, transform.position);
                    c = c - ((boid.transform.position - transform.position) * m);
                }
            }
        }

        return c;
    }



    Vector3 AdaptToVelocityOfGroup() 
    {
        Vector3 avgVelocityFlock = Vector3.zero;
        flockSize = 0;
        foreach (Boid boid in boids)
        {
            if (boid != this)
            {
                if (Vector3.Distance(this.transform.position, boid.transform.position) <= flockMaxDistance)
                {
                    flockSize++;
                    avgVelocityFlock += boid._velocity;
                }
            }
        }

        if (flockSize == 0)
        {
            return _velocity;
        }

        avgVelocityFlock = avgVelocityFlock / flockSize;

//        Debug.DrawLine(transform.position, transform.position+r3_perceivedVelocity, Color.yellow);
        return avgVelocityFlock;
    }



    Vector3 AvoidPredator() 
    {
        Vector3 c = Vector3.zero;

        foreach (Predator predator in predators)
        {
            float avoidDistance = (avoidPredatorBase * predator.AvoidMultiplier);
            if (Vector3.SqrMagnitude(predator.transform.position - this.transform.position) < avoidDistance * avoidDistance)
            {
                float m = avoidDistance - Vector3.Distance(predator.transform.position, this.transform.position);
                c = c - ((predator.transform.position - this.transform.position) * m );
            }
        }

//        Debug.DrawLine(transform.position, transform.position+c, Color.red);

        return c;
    }
}
