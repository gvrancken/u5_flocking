﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoidInfoText : MonoBehaviour {

    MouseManager mm;
    Text text;

    public Material selectedMat;

    // Use this for initialization
	void Start () {
        mm = GameObject.FindObjectOfType<MouseManager>();
        text = gameObject.GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (mm.selectedBoid)
        {
            text.text = mm.selectedBoid.transform.position.ToString() 
                + "\n" + mm.selectedBoid.flockSize;
        }
	}


}
